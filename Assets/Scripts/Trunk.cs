﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.treetrunks
{
	public interface IChoppable
	{
		int MaxChops { get; set; }
		int RemainingChops { get; set; }
		void GetChopped();
		bool HasBeenChopped();
	}

	public interface ITrunk : IChoppable
	{
		Transform NextTrunkTransform { get; }
		TreeController MyTreeController { get; set; }
	}

	/**
	* ************ Trunk *************
	* Author: Ahmed Almahdie
	* Date: 122519
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* controlls a single trunk in a tree
	* ****************************************/

    public class Trunk : MonoBehaviour, ITrunk
	{
		#region properties, variables, and declerations
		// marks the next trunk position
		[SerializeField] private Transform _nextTrunkTransform;
		public Transform NextTrunkTransform => _nextTrunkTransform;

		/// <summary>
		/// number of chops at which the trunk should be completely chopped.
		/// </summary>
		public int MaxChops { get; set; }

		/// <summary>
		/// remaining chops till the trunk is chopped.
		/// </summary>
		public int RemainingChops { get; set; }

		// ref for the treeController of this trunk
		public TreeController MyTreeController { get; set; }

		// invoked when the trunk is hit
		public UnityAction<Trunk> OnTrunkHit;
		#endregion

		#region public methods
		/// <summary>
		/// initializing the trunk's data, max and remaining chops
		/// </summary>
		/// <param name="maxChops"></param>
		public void Initialize(int maxChops = 0)
		{
			MaxChops = maxChops > 0 ? maxChops : 1;
			RemainingChops = MaxChops;
		}

		/// <summary>
		/// reducing num of remaining chops by one (for now)
		/// </summary>
		public void GetChopped()
		{
			if (RemainingChops - 1 < 0)
			{
				RemainingChops = 0;
			}
			else
			{
				RemainingChops--;
				OnTrunkHit?.Invoke(this);
			}
		}

		/// <summary>
		/// checks if a trunk has now remaining chops!
		/// </summary>
		/// <returns></returns>
		public bool HasBeenChopped()
		{
			return RemainingChops <= 0;
		}
		#endregion
	}
}
