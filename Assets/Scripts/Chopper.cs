﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.treetrunks
{
	public interface IChopper
	{
		bool CanChop { get; set; }
		ChoppingBladeSettings MyChoppingBladeSettings { get; set; }
		
		void Chop();
	}
	/**
	* ************ Chopper *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* responsible for firing up the chopper blades / raycast for now
	* ****************************************/

	public class Chopper : MonoBehaviour, IChopper
	{
		#region properties, variables, and declerations
		// blade settings
		[SerializeField] private ChoppingBladeSettings _myChoppingBladeSettings;
		public ChoppingBladeSettings MyChoppingBladeSettings
		{
			get { return _myChoppingBladeSettings; }
			set { _myChoppingBladeSettings = value; }
		}

		// the direction to shoot at
		[SerializeField] private Transform _aim;

		// allows the chopper to fire off a blade
		public bool CanChop { get; set; }

		// just keeping the blade's RateOfChopping cached in this variable
		private float _choppingCoolDownTime;
		// to track the rate of chopping time
		private float _timer;

		#endregion

		#region mono Methods
		private void Start()
		{
			initialize();
		}

		private void Update()
		{
			// tracking the cool down time
			if(_timer < _choppingCoolDownTime)
			{
				_timer += Time.deltaTime;
			}
			else
			{
				//if(!CanChop)
				//	Debug.Log("Can Chop Now");

				CanChop = true;
			}
		}
		#endregion

		#region Helper methods
		/// <summary>
		/// initializing the chopper's variables
		/// </summary>
		private void initialize()
		{
			// we only gonna use rateOfChopping from the settings object!
			_choppingCoolDownTime = _myChoppingBladeSettings.RateOfChopping;
			_timer = 0;
			CanChop = false;
		}
		#endregion

		#region public methods
		/// <summary>
		/// if the chopper canChop, it will fire a raycast at the defined aim direction, and 
		/// if it hits an IChoppable object, it will call its GetChopped method.
		/// we don't control the range of the raycase for now, but we should!
		/// </summary>
		public void Chop()
		{
			if (CanChop)
			{

				Debug.DrawRay(_aim.position, _aim.forward * 1000, Color.red, 10);

				RaycastHit _hit;
				if(Physics.Raycast(_aim.position, _aim.forward * 1000, out _hit))
				{
					if (_hit.transform.GetComponentInParent<IChoppable>() != null)
					{
						//Debug.Log("Hit: " + _hit.transform.name);
						_hit.transform.GetComponentInParent<IChoppable>().GetChopped();
					}
				}

				// to force the cool down again
				initialize();
			}
		}
		#endregion
	}
}
