﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.treetrunks
{
	public interface ISpawnSpot
	{
		Vector3 Position { get; }
		bool WasUsedBefore { get; set; }
	}

	/**
	* ************ SpawnSpot *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* This will be used to mark a Transform in the Scene
	* as a spawn position.
	* ****************************************/

    public class SpawnSpot : ISpawnSpot
	{
		#region properties, variables, and declerations
		/// <summary>
		/// spot's position in scene
		/// </summary>
		public Vector3 Position { get; protected set; }
		/// <summary>
		/// if this spot was taken before. I doubt that I'd use it to its fullest potential!
		/// but it seems needed anyway.
		/// </summary>
		public bool WasUsedBefore { get; set; }
		#endregion

		#region Constructor
		/// <summary>
		/// constructor to initialize position and wasUsedBefore variables
		/// </summary>
		/// <param name="position"></param>
		/// <param name="wasUsedBefore"></param>
		public SpawnSpot(Vector3 position, bool wasUsedBefore)
        {
			Position = position;
			WasUsedBefore = wasUsedBefore;
		}
        #endregion
    
        #region Helper methods

        #endregion

        #region public methods

        #endregion
    }
}
