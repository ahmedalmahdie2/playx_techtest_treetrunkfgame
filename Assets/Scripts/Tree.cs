﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.treetrunks
{
	public interface ITree
	{
		int TrunksNum { get; set; }
		int TrunkChops { get; set; }
		List<Trunk> MyTrunks { get; set; }
		bool HasBeenChopped();
	}

	/**
	* ************ Tree *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* stores a Tree's data in runtime
	* ****************************************/

	[System.Serializable]
	public class Tree : ITree
	{
		#region properties, variables, and declerations
		// the tree's gameObject
		public GameObject TreeObject;

		// the defined trunks number in this tree
		public int TrunksNum { get; set; }

		// stores the trunk chops from the TreeSettings scriptableObject
		public int TrunkChops { get; set; }
		// list of all Trunk instances in this tree
		public List<Trunk> MyTrunks { get; set; }
		#endregion

		#region Helper methods


		#endregion

		#region public methods
		public Tree(int trunksNum, int trunkChops, GameObject treeObject = null)
		{
			TrunksNum = trunksNum;
			TrunkChops = trunkChops;
			TreeObject = treeObject;
			MyTrunks = new List<Trunk>();
		}

		/// <summary>
		/// checking if the tree's trunks have been all chopped
		/// </summary>
		/// <returns></returns>
		public bool HasBeenChopped()
		{
			return MyTrunks.TrueForAll(x => x.HasBeenChopped());
		}
		#endregion
	}
}
