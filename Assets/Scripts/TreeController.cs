﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.treetrunks
{
	public interface ITreeController
	{
		Tree MyTree { get; set; }
	}

	/**
	* ************ TreeController *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* Controls the visual behaviour of the tree
	* ****************************************/

    public class TreeController : MonoBehaviour, ITreeController
	{
		#region properties, variables, and declerations
		[SerializeField] private GameObject _myTrunkPrefab; // the trunk prefab
		[SerializeField] private GameObject _myHitHighlight; // the hitHighlight Object in the tree
		[SerializeField] private Transform _trunksRoot; // the root transform of the tree

		// now, this should be absolutely linked with or calculated depending on the chopper's rate of chopping!
		// but, I reckon it would be a bit of an overdrive for the purposes of this phase of the game
		[SerializeField] private float _speedToPopupAndChopDown; // speed of poping up and chopping down animations

		// my Tree instance
		public Tree MyTree { get; set; }

		// for instantiating the trunks gameobjects
		private GameObject _tempTrunkObj;

		// to store the required distance to collapse the tree on trunk's hit
		private float _distanceToMoveTreeOnTrunkChopped;

		// invoked when the tree has finished it's popping out animation
		public UnityAction OnTreeFinishedPoppingout;

		// will be invoked when last trunk has consumed its chops
		public UnityAction OnTreeCompletelyChopped;
		#endregion
		
		#region Helper methods
		/// <summary>
		/// will be called for every trunk to subscribe to its hit events for now
		/// </summary>
		/// <param name="trunk"></param>
		private void subscribeToTrunkEvents(Trunk trunk)
		{
			trunk.OnTrunkHit += onTrunkWasHit;
		}
		/// <summary>
		/// unsubscribing from a trunks hit event
		/// </summary>
		/// <param name="trunk"></param>
		private void unsubscribeOfTrunkEvents(Trunk trunk)
		{
			trunk.OnTrunkHit -= onTrunkWasHit;
		}

		/// <summary>
		/// called when a trunk has invoked its hit event
		/// </summary>
		/// <param name="trunk"></param>
		private void onTrunkWasHit(Trunk trunk)
		{
			Debug.Log("onTrunkWasHit!");
			StartCoroutine(ChopTreeCoroutine());
		}

		/// <summary>
		/// distance from root position to the nextTrunkPosition on the first trunk
		/// which's the height of a single trunk, and divide that by the chops required per each trunk.
		/// </summary>
		/// <returns></returns>
		private float calculateDistanceToMoveOnTrunkChop()
		{
			var trunkHeight = Vector3.Distance(_trunksRoot.position, MyTree.MyTrunks[0].NextTrunkTransform.position);
			return trunkHeight / MyTree.TrunkChops;
		}
		#region Coroutines

		// here will handle the chopping animation of the tree
		IEnumerator ChopTreeCoroutine()
		{
			Debug.Log("begin ChopTreeCoroutine!");

			// show the hitHightlight object
			_myHitHighlight.SetActive(true);

			var targetPosition = _trunksRoot.position - _trunksRoot.up * _distanceToMoveTreeOnTrunkChopped;
			while (_trunksRoot.position != targetPosition)
			{
				_trunksRoot.position = Vector3.MoveTowards(_trunksRoot.position, targetPosition, Time.deltaTime * _speedToPopupAndChopDown);
				yield return null;
			}

			_trunksRoot.position = targetPosition;

			// highing the hitHightlight object againg
			_myHitHighlight.SetActive(false);
			Debug.Log("end ChopTreeCoroutine!");

			if(MyTree.HasBeenChopped())
			{
				OnTreeCompletelyChopped?.Invoke();
			}
		}
		/// <summary>
		/// will be used to popup an entire tree
		/// </summary>
		/// <returns></returns>
		IEnumerator PopoutCoroutine()
		{
			// ensure there is a trunk prefab
			if(!_myTrunkPrefab)
			{
				Debug.Log("Need to assign trunk prefab!");
				yield break;
			}

			// beginning with the first trunk position
			Vector3 nextTrunkPosition = _trunksRoot.position;

			// looping through all needed trunks to instantiate them
			for (int i = 0; i < MyTree.TrunksNum; i++)
			{
				// if this is not the very first trunk! in which case
				// we would need the next trunk's position from this trunk
				if (_tempTrunkObj)
				{
					nextTrunkPosition = _tempTrunkObj.GetComponent<Trunk>().NextTrunkTransform.position;
				}

				// we couble obviously play with the rotation here, but, let's focus on the simple things first
				_tempTrunkObj = Instantiate(_myTrunkPrefab, nextTrunkPosition, Quaternion.identity, _trunksRoot);
				// passing number of chops per trunk to the trunk instance
				_tempTrunkObj.GetComponent<Trunk>().Initialize(MyTree.TrunkChops);
				// subscribing to hit events on the trunk
				subscribeToTrunkEvents(_tempTrunkObj.GetComponent<Trunk>());
				// finally, adding the trunk to the tree's Trunks List
				MyTree.MyTrunks.Add(_tempTrunkObj.GetComponent<Trunk>());
				yield return null;
			}

			// at the end, calculating distance to move when chopping a trunk
			_distanceToMoveTreeOnTrunkChopped = calculateDistanceToMoveOnTrunkChop();

			OnTreeFinishedPoppingout?.Invoke();
		}
		#endregion

		#endregion

		#region public methods
		public void PopoutTree()
		{
			StartCoroutine(PopoutCoroutine());
		}
        #endregion
    }
}
