﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace mahdie.playx.treetrunks
{
	/**
	* ************ TreeGenerator *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* Searches for all spawn spots in the scene,
	* picks one up, then spawns a tree at that position.
	* ****************************************/

    public class TreeGenerator : MonoBehaviour
	{
		#region properties, variables, and declerations
		/// <summary>
		/// the tree stats scriptableObject.
		/// </summary>
		[SerializeField] private TreeSettings _myTreeData;

		// array for all spawnspots in the scene, used and unused
		public SpawnSpot[] MySpawnSpots { get; private set; }

		// will be used as the initial spawn spot!
		private SpawnSpot _currentSpawnSpot;
		// this will be used to exclude the current spawn spot when generating a new tree
		private List<SpawnSpot> _ListOfAvailableSpawnSpots;

		// will be used for inistantiating the trees
		private GameObject _myCurrentTreeObject;

		// to use when the we need to move the camera to new tree position
		private CameraMovementHandler cameraHandler;
		// to account for the difference between where the spawnspot is,
		// and the camera's position which get's the tree to be inside 
		// the camera's projection
		private Vector3 cameraOffset;
		#endregion

		#region mono methods
		private void Awake()
		{
			setupDependencies();
		}

		private void Start()
		{
			initialize();
		}
		#endregion

		#region Helper methods
		/// <summary>
		/// looks for all spawnspots in the scene and stores them into a defined array
		/// then takes a random initial one of them as the first spawnspot
		/// </summary>
		private void setupDependencies()
		{
			// getting the cameramovementHandler ref in the scene
			cameraHandler = FindObjectOfType<CameraMovementHandler>();

			// Initializing the spawnspots pool from objects with SpawnSpot tag in scene
			var spawnSpotsObjs = GameObject.FindGameObjectsWithTag("SpawnSpot");

			// we are adding one to account for the initial spawn spot
			// which is the transform of this gameObject!
			MySpawnSpots = new SpawnSpot[spawnSpotsObjs.Length + 1];
			MySpawnSpots[0] = new SpawnSpot(transform.position, false);

			// "i" will begin from 0 since we're referencing spawnSpotsObjs array inside the loop!
			for (int i = 0; i < spawnSpotsObjs.Length; i++)
			{
				// "i+1" not to overwite the initial spawnspot we added before the lopp.
				// position of the transform in scene, and false for WasUsedBefore flag.
				MySpawnSpots[i+1] = new SpawnSpot(spawnSpotsObjs[i].transform.position, false);
			}
		}

		/// <summary>
		/// initializing all variables that matter to tree generation process
		/// </summary>
		private void initialize()
		{
			// getting a the very first spot as an the initial spawnspot
			_currentSpawnSpot = MySpawnSpots[0];
			// and marking it so that it won't be used again if we wanted to.
			_currentSpawnSpot.WasUsedBefore = true;
			
			// calculating the offset to be always like that of the camera position
			// in relation to the initial spawnspot
			cameraOffset = cameraHandler.transform.position - _currentSpawnSpot.Position;

			// here we go, initializing the spawnSpots pool and treeObject.
			_ListOfAvailableSpawnSpots = new List<SpawnSpot>();
			_myCurrentTreeObject = null;

			// generating the first tree
			GenerateNewTree(_currentSpawnSpot);
		}

		/// <summary>
		/// it will get a randon spot that's unused if needed. Also, it will be default make sure the new spot
		/// is not same as the current one.
		/// </summary>
		/// <param name="potentialSpots"></param>
		/// <param name="includeUsedSpots"></param>
		/// <returns></returns>
		private SpawnSpot getRandomSpawnSpotFromList(SpawnSpot[] potentialSpots, bool includeUsedSpots = false)
		{
			// initializing the spawn spots pool
			if (_ListOfAvailableSpawnSpots == null)
			{
				_ListOfAvailableSpawnSpots = new List<SpawnSpot>();
			}
			else if (_ListOfAvailableSpawnSpots.Count > 0)
			{
				// making sure the pool is empty for this iteration
				_ListOfAvailableSpawnSpots.Clear();
			}
			_ListOfAvailableSpawnSpots.AddRange(potentialSpots);

			//removing all used spots from the pool.
			if (!includeUsedSpots)
				_ListOfAvailableSpawnSpots.RemoveAll(x => x.WasUsedBefore);

			// to get rid of the current spawnspot of course
			_ListOfAvailableSpawnSpots.RemoveAll(x => x.Position == _currentSpawnSpot.Position);

			// just to make sure there are unused spots in the pool
			if (_ListOfAvailableSpawnSpots.Count > 0)
				return _ListOfAvailableSpawnSpots[Random.Range(0, _ListOfAvailableSpawnSpots.Count)];
			else
				return null;
		}

		/// <summary>
		/// Instantiates a tree prefab onto the spot defined
		/// </summary>
		/// <param name="spot"></param>
		private void generateNewTreeAt(SpawnSpot spot)
		{
			if(_myTreeData.TreePrefab)
			{
				var position = spot.Position;
				// this makes sure that we have the tree position in the right y position as the treePrefab
				// I don't like it! but it will work for now
				position.y = _myTreeData.TreePrefab.transform.position.y;

				// instantiating the tree gameObject
				_myCurrentTreeObject = Instantiate(_myTreeData.TreePrefab, position, _myTreeData.TreePrefab.transform.rotation, transform);

				// making a new tree with the stats defined in he treeData scriptableObject that we have
				// here we also get a random number of trunks depending on the treeData limits
				_myCurrentTreeObject.GetComponent<TreeController>().MyTree = new Tree(Random.Range(_myTreeData.MinTrunksNum, _myTreeData.MaxTrunksNum + 1), _myTreeData.TrunkChops, _myCurrentTreeObject);
				_myCurrentTreeObject.GetComponent<TreeController>().PopoutTree();
				_myCurrentTreeObject.GetComponent<TreeController>().OnTreeFinishedPoppingout += OnTreePoppedOut;
				_myCurrentTreeObject.GetComponent<TreeController>().OnTreeCompletelyChopped += OnTreeChopped;
			}
		}

		/// <summary>
		/// begin handling of a new tree sequence
		/// </summary>
		private void OnTreeChopped()
		{
			Debug.Log("OnTreeChopped");
			_myCurrentTreeObject.GetComponent<TreeController>().OnTreeCompletelyChopped -= OnTreeChopped;
			StartCoroutine(PrepareForNewTreeCoroutine());
		}

		/// <summary>
		/// It will mark the time that a tree has finished popping out
		/// so that we could maybe move the camera there if we need to for example
		/// </summary>
		private void OnTreePoppedOut()
		{
			Debug.Log("OnTreePoppedOut");
			_myCurrentTreeObject.GetComponent<TreeController>().OnTreeFinishedPoppingout -= OnTreePoppedOut;
			cameraHandler.MoveToPosition(_currentSpawnSpot.Position + cameraOffset);
		}

		#region Coroutines
		/// <summary>
		/// we could have some animation done here, so, it is a coroutine for now!
		/// </summary>
		/// <returns></returns>
		IEnumerator PrepareForNewTreeCoroutine()
		{
			Debug.Log("PrepareForNewTreeCoroutine");
			yield return null;
			GenerateNewTree();
		}
		#endregion
		#endregion

		#region public methods

		/// <summary>
		/// begins the process of generating a new tree at some spawn spot
		/// </summary>
		/// <param name="spawnSpot"></param>
		/// <returns></returns>
		public bool GenerateNewTree(SpawnSpot spawnSpot = null)
		{
			// special case, where we did define the specific spawn spot we want for the new tree
			// this case will mostly be used for generating the first tree, at least throughout the scope of this phase of the game!
			if(spawnSpot != null)
			{
				generateNewTreeAt(_currentSpawnSpot = spawnSpot);
				return true;
			}

			// here, we try to create a new tree from the spawnspots pool available
			if(MySpawnSpots.Length > 0)
			{
				// getting a new spawnSpot from the pool
				var tempSpot = getRandomSpawnSpotFromList(MySpawnSpots);
				
				if (tempSpot != null)
				{
					_currentSpawnSpot = tempSpot;
					_currentSpawnSpot.WasUsedBefore = true;
					generateNewTreeAt(_currentSpawnSpot);
					return true;
				}
				else
				{
					// so, no unused spots! just so that we won't get stuck looking for 
					tempSpot = getRandomSpawnSpotFromList(MySpawnSpots, true);

					// I'm not going to seperate this check into another method.
					// I can just repeat here this one time, but will seperate it if I had to use it another time!
					if (tempSpot != null)
					{
						_currentSpawnSpot = tempSpot;
						_currentSpawnSpot.WasUsedBefore = true;
						generateNewTreeAt(_currentSpawnSpot);
						return true;
					}
				}
			}

			// no spawn spot was found!
			Debug.Log("something went wrong when trying to look for new Spawn Spots! It's definitely the level Designer's fault!!! :)");
			return false;
		}
		#endregion
	}
}
