﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.treetrunks
{
	public interface IChoppingBlade
	{
		int BladePower { get; set; }
		bool IsConsumed { get; set; }
	}
	/**
	* ************ ChoppingBlade *************
	* Author: Ahmed Almahdie
	* Date: 122619
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* it could control a chopper's blade if we're to spawn one!
	* ****************************************/

    public class ChoppingBlade : MonoBehaviour, IChoppingBlade
	{
		#region properties, variables, and declerations
		public int BladePower { get; set; }
		public bool IsConsumed { get; set; }
		#endregion

		#region MonoBehaviour methods
		// Start is called before the first frame update
		void Start()
        {
            
        }

		private void OnTriggerEnter(Collider other)
		{
			if(other.GetComponentInParent<IChoppable>() != null)
			{
				other.GetComponentInParent<IChoppable>().GetChopped();
			}
		}
		#endregion

		#region Helper methods

		#endregion

		#region public methods

		#endregion
	}
}
