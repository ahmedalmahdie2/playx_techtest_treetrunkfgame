﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie.playx.treetrunks
{
	/**
	* ************ TreeSettings *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* Defines the tree's prefab, min and max trunks count
	* ****************************************/
	
	[CreateAssetMenu (menuName = "Single Instances/New Tree Scriptable")]
	public class TreeSettings : ScriptableObject
	{
		#region properties, variables, and declerations
		[SerializeField] private GameObject _treePrefab;
		public GameObject TreePrefab => _treePrefab;

		[SerializeField] private int _minTrunksNum;
		public int MinTrunksNum => _minTrunksNum;

		[SerializeField] private int _maxTrunksNum;
		public int MaxTrunksNum => _maxTrunksNum;

		// how many chops needed to chop the whole TRUNK, NOT TREE, THE TRUNK!
		[SerializeField] private int _trunkChops;
		public int TrunkChops => _trunkChops;
		#endregion
	}
}
