﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace mahdie
{
	/**
	* ************ ChoppingBladeSettings *************
	* Author: Ahmed Almahdie
	* Date: 122619
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* defines the blade stats, mainly the prefab, power, and rate of chopping
	* ****************************************/
	[CreateAssetMenu(menuName = "Single Instances/New Blade Scriptable")]
	public class ChoppingBladeSettings : ScriptableObject
	{
		#region properties, variables, and declerations
		[SerializeField] private GameObject _bladePrefab;
		public GameObject BladePrefab => _bladePrefab;

		[SerializeField] private int _bladePower;
		public int BladePower => _bladePower;

		[SerializeField] [Range (0, 1)] private float _rateOfChopping;
		public float RateOfChopping => _rateOfChopping;
		#endregion
	}
}
