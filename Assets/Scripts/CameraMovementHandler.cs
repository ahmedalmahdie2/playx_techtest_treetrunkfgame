﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace mahdie.playx.treetrunks
{
	public interface ICameraMovementHandler
	{
		Transform MovementRoot { get; }
		Vector3 TargetPosition { get; set; }
		bool HasArrived { get; }
		float Speed { get; }
		void MoveToPosition(Vector3 target);
	}

	/**
	* ************ CameraMovementHandler *************
	* Author: Ahmed Almahdie
	* Date: 122419
	* Owner: Ahmed Almahdie
	* ************ Discription: *************
	* It will handle moving camera to its target position which will be passed on to it as a variable
	* ****************************************/

	public class CameraMovementHandler : MonoBehaviour, ICameraMovementHandler
	{
		#region properties, variables, and declerations
		// this is just to enable us to add this monobehaviour to any gameObject and have it 
		// move any other transform.
		// it will be initialized to this transform if not set in the inspector.
		[SerializeField] private Transform _movementRoot;
		public Transform MovementRoot => _movementRoot;

		// the speed in which the camera will move to its target
		[SerializeField] private float _speed;
		public float Speed => _speed;

		// It's self explainatory really, but I was tempted to add some kine of 
		// an offset that we can define in the inspector, but I think it'd better to let 
		// maybe the Levelmanager handle the target position of the camera entirely
		public Vector3 TargetPosition { get; set; }

		// marking the period when the camera is moving to the target and hasn't arrived yet
		public bool HasArrived { get; private set; }

		// will be invoked when the camera transform reaches its target position
		public UnityAction OnArrivedAtTargetPosition;
		#endregion

		#region MonoBehaviour methods
		private void Awake()
		{
			SetupDependencies();
		}
		// Start is called before the first frame update
		void Start()
        {
			initialize();
		}
    
        // Update is called once per frame
        void Update()
        {
            if(!HasArrived)
			{
				// checking if hasn't arrived at the target position
				if(!hasArrivedAtTargetPosition())
				{
					// moving the root to the target position with the defined speed;
					_movementRoot.position = Vector3.MoveTowards(_movementRoot.position, TargetPosition, Time.deltaTime * _speed);
				}
				else
				{
					// has arrived, invoking OnArrivedAtTargetPosition for all listeners
					HasArrived = true;
					OnArrivedAtTargetPosition?.Invoke();
				}
			}
        }
		#endregion

		#region Helper methods
		private void SetupDependencies()
		{
			// initializing the root to this transform if non was assigned in the inspector
			if (!_movementRoot)
				_movementRoot = transform;
		}

		private void initialize()
		{
			// setting hasArrived to true!
			HasArrived = true;
		}

		// checking the distance to the target position if it's not bigger than 0
		// I don't like this method! but let's roll with it for now!
		private bool hasArrivedAtTargetPosition()
		{
			return Vector3.Distance(_movementRoot.position, TargetPosition) <= 0;
		}
        #endregion

        #region public methods
		// to set the new target position and get the camera to move towards it
		public void MoveToPosition(Vector3 target)
		{
			HasArrived = false;
			TargetPosition = target;
		}
        #endregion
    }
}
